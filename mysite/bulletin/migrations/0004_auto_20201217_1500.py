# Generated by Django 3.1.4 on 2020-12-17 07:00

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('bulletin', '0003_user'),
    ]

    operations = [
        migrations.CreateModel(
            name='Roles',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('is_banned', models.BooleanField(default=False)),
                ('user_role', models.CharField(choices=[('Admin', 'Admin'), ('Moderator', 'Moderator'), ('Poster', 'Poster')], default='Poster', max_length=9)),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.RemoveField(
            model_name='moderator',
            name='moderator',
        ),
        migrations.RemoveField(
            model_name='poster',
            name='poster',
        ),
        migrations.DeleteModel(
            name='User',
        ),
        migrations.DeleteModel(
            name='Moderator',
        ),
        migrations.DeleteModel(
            name='Poster',
        ),
    ]
