from .views import HomePageView, ModifyBoardsView, BoardIndexView, ThreadIndexView, UserPageView
from django.urls import path

from . import views

urlpatterns = [
    path('', HomePageView.as_view(), name='bulletin-home'),
    path('profile/<str:name>', UserPageView.as_view(), name='user_page'),
    path('boards/modify_boards', ModifyBoardsView.as_view(), name='modify_boards'),
    path('boards/<int:pk>', BoardIndexView.as_view(), name='board_index'),
    path('boards/threads/<int:pk>', ThreadIndexView.as_view(), name='thread_index')
]
