from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone


class Board(models.Model):
    title = models.CharField(max_length=100)
    creator = models.ForeignKey(User, on_delete=models.CASCADE)
    created_on = models.DateTimeField(auto_now_add=True)
    position = models.PositiveIntegerField()

    def __str__(self):
        return self.title

    def last_post(self):
        if self.thread_set.count():
            last = None
            for t in self.thread_set.all():
                l = t.last_post()
                if l:
                    if not last:
                        last = l
                    elif l.created_on > last.created_on:
                        last = l
            return last

    def no_of_threads(self):
        num_of_threads = self.thread_set.all().count()
        return num_of_threads

    def no_of_posts(self):
        threads = self.thread_set.all()
        num_of_posts = 0
        for thread in threads:
            num_of_posts += thread.post_set.all().count()
        return num_of_posts


class Thread(models.Model):
    title = models.CharField(max_length=100)
    board = models.ForeignKey(Board, on_delete=models.CASCADE)
    creator = models.ForeignKey(User, on_delete=models.CASCADE)
    created_on = models.DateTimeField(auto_now_add=True)
    is_locked = models.BooleanField(blank=False, default=False)

    def __str__(self):
        return self.title

    def last_post(self):
        if self.post_set.count():
            return self.post_set.order_by("created_on")[0]

    def last_reply_date(self):
        recent_post = self.post_set.order_by("-created_on").first()
        if not recent_post:
            recent_post_date = None
        else:
            recent_post_date = recent_post.created_on
        return recent_post_date

    def last_reply_name(self):
        recent_post = self.post_set.order_by("-created_on").first()
        if not recent_post:
            recent_post_creator = None
        else:
            recent_post_creator = recent_post.creator
            return recent_post_creator


class Post(models.Model):
    message = models.TextField()
    thread = models.ForeignKey(Thread, on_delete=models.CASCADE)
    creator = models.ForeignKey(User, on_delete=models.CASCADE)
    created_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.message


class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    is_banned = models.BooleanField(blank=False, default=False)

    ROLE_CHOICES = [
        ('Admin', 'Admin'),
        ('Moderator', 'Moderator'),
        ('Poster', 'Poster')
    ]

    role = models.CharField(
        max_length=9,
        choices=ROLE_CHOICES,
        default='Poster'
    )
