from bulletin.models import Board, Thread, Post, UserProfile
from django.contrib import admin

admin.site.register(Board)
admin.site.register(Thread)
admin.site.register(Post)
admin.site.register(UserProfile)