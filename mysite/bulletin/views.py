from .models import Board, Thread, Post, UserProfile
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from django.utils import timezone
from django.views.generic.base import View, TemplateView


class HomePageView(TemplateView):
    template_name = "bulletin/home.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        # Check user type of visiting user.
        if self.request.user.is_authenticated:
            user_type = UserProfile.objects.get(user=self.request.user).role
            if user_type == 'Admin':
                is_admin = True
        else:
            is_admin = False

        context['is_admin'] = is_admin

        # Call each board and its most recent thread
        boards_list = Board.objects.order_by('position')
        recent_thread_list = []

        for board in boards_list:

            if board.thread_set.all():
                recent_thread_list.append(
                    board.thread_set.latest('created_on')
                )
            else:
                recent_thread_list.append(
                    False
                )

        context['board_and_recent_thread'] = zip(boards_list, recent_thread_list)
        return context


class UserPageView(View):
    template_name = 'bulletin/user_page.html'

    def get(self, request, name):
        # Get user type of user to determine if ban/unban button will be shown.
        user_type = UserProfile.objects.get(user=self.request.user).role

        if user_type == 'Admin' or user_type == 'Moderator':
            is_admin_or_mod = True
        else:
            is_admin_or_mod = False

        # Display name of user of interest and their corresponding posts.
        if name != 'None':
            user_profile = User.objects.get(username=name)
            user_name = user_profile.username
            is_banned = UserProfile.objects.get(
                user=user_profile
            ).is_banned
            posts = user_profile.post_set.order_by('-created_on')

        else:
            user_name = False
            is_banned = False
            posts = False

        context = {
            'is_admin_or_mod': is_admin_or_mod,
            'user_name': user_name,
            'is_banned': is_banned,
            'posts': posts
        }

        return render(self.request, self.template_name, context)

    def post(self, request, name):
        # Process ban/unban request.
        if self.request.POST.get('ban'):
            user = User.objects.get(username=name)
            if self.request.POST.get('ban') == 'ban':
                UserProfile.objects.filter(
                    user=user
                ).update(
                    is_banned=True
                )
            else:
                UserProfile.objects.filter(
                    user=user
                ).update(
                    is_banned=False
                )
        return HttpResponseRedirect(reverse('user_page', args=[name]))


class ModifyBoardsView(View):
    template_name = "bulletin/modify_boards.html"

    def get(self, request):
        # Display all boards.
        boards = Board.objects.all()
        # Check user type.
        user_type = UserProfile.objects.get(user=self.request.user).role

        if user_type == 'Admin':
            is_admin = True
        else:
            is_admin = False

        context = {
            'boards': boards,
            'is_admin': is_admin
        }
        return render(self.request, self.template_name, context)

    def post(self, request):
        # Create boards.
        if self.request.POST.get('board_create'):
            # Check if table is populated.
            if Board.objects.all():
                last_position = Board.objects.latest('created_on').position
            else:
                last_position = 0
            Board(
                title=self.request.POST.get('board_create'),
                creator=User.objects.get(username=self.request.user.username),
                created_on=timezone.now(),
                position=last_position + 1
            ).save()

        # Remove boards.
        if self.request.POST.get('board_remove'):
            board = Board.objects.get(id=self.request.POST.get('board_remove'))
            board.delete()

        return HttpResponseRedirect(reverse('modify_boards'))


class BoardIndexView(View):
    template_name = 'bulletin/board_index.html'

    def get(self, request, pk):
        # Restrict actions depending on user type.
        user_type = UserProfile.objects.get(user=self.request.user).role

        if user_type == 'Admin' or user_type == 'Moderator':
            is_admin_or_mod = True
        else:
            is_admin_or_mod = False

        if UserProfile.objects.get(user=self.request.user).is_banned:
            is_banned = True
        else:
            is_banned = False

        # Prepare board and its corresponding threads.
        board = Board.objects.get(pk=pk)
        threads = board.thread_set.order_by('-created_on')

        context = {
            'is_admin_or_mod': is_admin_or_mod,
            'is_banned': is_banned,
            'board': board,
            'threads': threads
        }

        return render(self.request, self.template_name, context)

    def post(self, request, pk):
        # Create threads.
        if self.request.POST.get('thread'):
            Thread(
                title=self.request.POST.get('thread'),
                board=Board.objects.get(pk=pk),
                creator=self.request.user,
                created_on=timezone.now(),
                is_locked=False
            ).save()

        # Lock or unlock threads.
        if self.request.POST.get('Lock/Unlock'):
            thread = Thread.objects.filter(
                id=int(self.request.POST.get('thread_id'))
            )

            if self.request.POST.get('Lock/Unlock') == 'lock':
                thread.update(
                    is_locked=True
                )
            else:
                thread.update(
                    is_locked=False
                )

        return HttpResponseRedirect(reverse('board_index', args=[pk]))


class ThreadIndexView(View):
    template_name = 'bulletin/thread_index.html'

    def get(self, request, pk):
        # Check user type and restrictions.
        is_banned = UserProfile.objects.get(user=self.request.user).is_banned
        user_type = UserProfile.objects.get(user=self.request.user).role

        if user_type == 'Admin' or user_type == 'Moderator':
            is_admin_or_mod = True
        else:
            is_admin_or_mod = False

        # Check thread and its corresponding boards/posts.

        thread = Thread.objects.get(pk=pk)
        board = thread.board
        posts = thread.post_set.order_by('created_on')

        context = {
            'is_banned': is_banned,
            'is_admin_or_mod': is_admin_or_mod,
            'board': board,
            'thread': thread,
            'posts': posts
        }

        return render(self.request, self.template_name, context)

    def post(self, request, pk):
        # Obtain reply to a thread.
        if self.request.POST.get('message'):
            message = self.request.POST.get('message')
            creator = self.request.user
            created_on = timezone.now()
            Post(
                message=message,
                thread=Thread.objects.get(pk=pk),
                creator=creator,
                created_on=created_on
            ).save()

        return HttpResponseRedirect(reverse('thread_index', args=[pk]))
